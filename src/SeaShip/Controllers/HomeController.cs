﻿using System;
using Microsoft.AspNetCore.Mvc;
using SeaShip.Models;

namespace SeaShip.Controllers
{
    /// <summary>
    /// Default Controller for app, lands on index page on browse, launches user into app of carton calculator
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Default get method for hitting index
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            IndexFormModel form = new IndexFormModel();
            return View(form);
        }

        /// <summary>
        /// On form post from index controller
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index([Bind("containerWidth, containerHeight, containerLength, cartons")]IndexFormModel form)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    form.cartons = GetCartonsThatCanFit(form.containerLength, form.containerWidth, form.containerHeight);
                }
                catch(Exception e)
                {
                    throw;
                }
            }
            return View(form);
        }

        /// <summary>
        /// Method that takes Length, Width, Height params as doubles,
        /// Interpret the values as feet by converting to inches *12,
        /// And dividing them by fixed carton size of 18x18x8,
        /// To return the closest integer value of number of cartons that can fit
        /// </summary>
        /// <param name="containerLength"></param>
        /// <param name="containerWidth"></param>
        /// <param name="containerHeight"></param>
        /// <returns></returns>
        private int GetCartonsThatCanFit(double containerLength, double containerWidth, double containerHeight)
        {
            //s1 convert user input to inches, to handle w/ cartons 12" in 1'
            double contHighInch = containerHeight * 12;
            double contWideInch = containerWidth * 12;
            double contLongInch = containerLength * 12;

            int cartonsHigh = (int)Math.Floor(contHighInch / 8);//how many cartons can we stack (bot to top)
            int cartonsWide = (int)Math.Floor(contWideInch / 18);//how many cartons beside eachother (side to side)
            int cartonsLong = (int)Math.Floor(contLongInch / 18);//how many cartons in a row (end to end)

            return cartonsHigh * cartonsWide * cartonsLong;
        }

        /// <summary>
        /// Method that displays a view that holds what the app is about
        /// </summary>
        /// <returns></returns>
        public IActionResult About()
        {
            return View();
        }

        /// <summary>
        /// Method that holds my email and the repo for the source of the app
        /// </summary>
        /// <returns></returns>
        public IActionResult Contact()
        {
            return View();
        }

        /// <summary>
        /// ASPNET/MVC provided method for displaying error(s)
        /// </summary>
        /// <returns></returns>
        public IActionResult Error()
        {
            return View();
        }
    }
}
