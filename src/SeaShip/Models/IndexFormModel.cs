﻿/*
 * Filename: IndexFormModel.cs
 * 
 * Description: This file will hold model properties for the form that will be displayed on the index page and be posted back to the POST method
 * 
 * Version 1.0
 * 
 * $History
 * 
 * 16/09/2016
 * Version 1.0
 * Skylar Barth wsbarth92@gmail.com
 * Initial
 * 
 * */

using System.ComponentModel.DataAnnotations;

namespace SeaShip.Models
{
    public class IndexFormModel
    {
        [Display(Name = "Container Width")]
        public double containerWidth { get; set; }

        [Display(Name = "Container Height")]
        public double containerHeight { get; set; }

        [Display(Name = "Container Length")]
        public double containerLength { get; set; }

        [Display(Name = "Cartons")]
        public int cartons { get; set; }
    }
}
